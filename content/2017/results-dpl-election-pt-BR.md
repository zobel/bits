Title: Eleição 2017 DPL, parabéns Chris Lamb!
Date: 2017-04-16 18:40
Tags: dpl
Slug: results-dpl-elections-2017
Author: Laura Arjona Reina
Status: published
Lang: pt-BR
Translator: Paulo Henrique de Lima Santana

A eleição para o Líder do Projeto Debian terminou ontem e o vencedor foi Chris Lamb!

De um total de 1062 desenvolvedores, 322 desenvolvedores votaram usando o [Método Condorcet](http://en.wikipedia.org/wiki/Condorcet_method).

Mais informações sobre o resultado estão disponíveis na [página de eleição 2017 do Líder do Projeto Debian](https://www.debian.org/vote/2017/vote_001).

O atual Lider do Projeto Debian, Mehdi Dogguy, parabenizou Chris Lamb nos seus
[Bits finais na mensagem do DPL (de saída)](https://lists.debian.org/debian-project/2017/04/msg00041.html).
Obrigado, Mehdi, por seus serviços prestados como DPL durante os últimos doze meses!

O novo mandato do Líder do projeto inicia em 17 de abril e termina em 16 de abril de 2018.

