Title: Savoir-faire Linux Platinum Sponsor of DebConf17
Slug: sfl-platinum-debconf17
Date: 2017-01-30 17:50
Author: Laura Arjona Reina and Tássia Camões Araújo
Tags: debconf17, debconf, sponsors, Savoir-faire Linux
Status: published

[![SFLlogo](|filename|/images/savoirfairelinux.svg)](https://www.savoirfairelinux.com/)

We are very pleased to announce that [**Savoir-faire Linux**](https://www.savoirfairelinux.com/)
has committed support to [DebConf17](http://debconf17.debconf.org) as a **Platinum sponsor**.

*"Debian acts as a model for both Free Software and developer
communities. Savoir-faire Linux promotes both vision and values of
Debian. Indeed, we believe that it's an essential piece, in a social
and political way, to the freedom of users using modern technological
systems"*, said Cyrille Béraud, president of Savoir-faire Linux.


Savoir-faire Linux is a Montreal-based Free/Open-Source Software company
with offices in Quebec City, Toronto, Paris and Lyon. It offers Linux and
Free Software integration solutions in order to provide performance,
flexibility and independence for its clients. The company actively contributes
to many free software projects, and provides mirrors of Debian, Ubuntu, Linux
and others.

Savoir-faire Linux was present at DebConf16 program with a talk about Ring, its
GPL secure and distributed communication system. Ring package was accepted in
Debian testing during DebCamp in 2016 and will be part of Debian Stretch.
OpenDHT, the distributed hash table implementation used by Ring, also appeared
in Debian experimental during last DebConf.

With this commitment as Platinum Sponsor,
Savoir-faire Linux contributes to make possible our annual conference,
and directly supports the progress of Debian and Free Software
helping to strengthen the community that continues to collaborate on
Debian projects throughout the rest of the year.

Thank you very much Savoir-faire Linux, for your support of DebConf17!

## Become a sponsor too!

DebConf17 is still accepting sponsors.
Interested companies and organizations may contact the DebConf team
through [sponsors@debconf.org](mailto:sponsors@debconf.org), and
visit the DebConf17 website at [http://debconf17.debconf.org](http://debconf17.debconf.org).
