Title: L'instal·lador de Tails ja és a Debian
Slug: tails-installer-in-debian
Date: 2016-02-11 14:30
Author: u
Translator: Adrià García-Alzórriz
Lang: ca
Tags: tails, privacy, anonymity, announce
Status: draft

[Tails][tails-link] (The amnesic incognito live system) és un sistema
operatiu «Live» basat en Debian GNU/Linux, la finalitat del qual és 
mantenir la privadesa i l'anonimat dels seus usuaris, usant Internet de
forma anònima i eludint la censura. S'instal·la en una memòria USB i
es configura per tal que no deixi cap rastre a l'ordinador en el que
s'usi a no ser que li demani explícitament. 
[![Tails Logo](|filename|/images/tails-logo-flat.png)][tails-link]

A partir d'avui, aquells que necessitin seguretat digital no caldrà que
siguin experts en informàtica. És essencial que a l'hora d'adoptar una
nova eina, aquesta sigui fàcil d'aprendre fins i tot en entorns plens
d'estrès o de risc. Aquest és el motiu pel qual hem volgut fer que la
instal·lació de Tails pels nous usuaris sigui més ràpida, simple i 
segura.

Un dels components de Tails, l'[instal·lador de Tails][tails-instaler]
es troba ara a Debian gràcies a l'[Equip de mantenidors d'eines de privadesa de Debian][team-link].

L'instal·lador de Tails és una [eina gràfica][screenshot] per 
instal·lar o actualitzar Tails en una memòria USB des d'una imatge ISO.
La finalitat és tenir [Tails preparat i en marxa][tails-installdoc] de forma més fàcil i ràpida..

El procés anterior per iniciar-se en Tails era molt complex i 
problemàtic per aquells menys tècnics. Es necessitava iniciar Tails 
tres vegades, i copiar la imatge ISO sencera a la memòria USB dues 
vegades abans de tenir-lo plenament funcional amb la persistència
habilitada.

Ara es pot fer de forma més senzilla simplement instal·lant 
l'Instal·lador de Tails al seu sistema Debian existent, usant Sid, 
Stretch o jessie-backports, connectant una memòria USB i escollint si 
voleu actualitzar la memòria USB o bé instal·lar-hi Tails a partir
d'una imatge ISO prèviament descarregada.

L'Instal·lador de Tails també ajuda els actuals usuaris de Tails a
crear un espai d'emmagatzemament persistent xifrat per desar-hi fitxers
personals i configuracions a l'espai sobrant de la memòria USB.

[tails-link]: https://tails.boum.org
[tails-instaler]: https://tracker.debian.org/pkg/tails-installer
[screenshot]: https://screenshots.debian.net/package/tails-installer
[team-link]: https://qa.debian.org/developer.php?email=pkg-privacy-maintainers%40lists.alioth.debian.org
[tails-installdoc]: https://tails.boum.org/install


