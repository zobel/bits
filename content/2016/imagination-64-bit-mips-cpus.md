Title: Imagination accelerates Debian development for 64-bit MIPS CPUs
Slug: imagination-64-bit-mips-cpus
Date: 2016-05-18 09:30
Author: Laura Arjona Reina
Tags: imagination, donation, mips, debian
Status: published

[Imagination  Technologies][1] recently donated several high-performance SDNA-7130  appliances to the Debian Project for the development and maintenance of  the [MIPS ports][6].

The  SDNA-7130 (Software Defined Network Appliance) platforms are developed  by [Rhino Labs][2], a leading provider of high-performance data  security, networking, and data infrastructure solutions.

With these  new devices, the Debian project will have access to a wide range of 32- and 64-bit MIPS-based platforms.

Debian  MIPS ports are also possible thanks to donations from the [aql][3]  hosting service provider, the Eaton remote controlled ePDU, and many  other individual members of the Debian community.

The Debian project would like to thank Imagination, Rhino Labs and aql for this coordinated donation.

More  details about GNU/Linux for MIPS CPUs can be found in the [related  press release at Imagination][4] and their [community site about  MIPS][5].

[1]: http://www.imgtec.com
[2]: http://www.rhinolabsinc.com
[3]: http://www.aql.com
[4]: https://imgtec.com/news/press-release/imagination-accelerates-debian-development-64-bit-mips-cpus/
[5]: https://community.imgtec.com/developers/mips/linux
[6]: https://www.debian.org/ports/mips/
