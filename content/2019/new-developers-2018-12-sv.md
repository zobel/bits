Title: Nya Debianutvecklare och Debian Maintainers (November och December 2018)
Slug: new-developers-2018-12
Date: 2019-01-08 14:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published


Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två månaderna:

  * Abhijith PA (abhijith)
  * Philippe Thierry (philou)
  * Kai-Chung Yan (seamlik)
  * Simon Qhuigley (tsimonq2)
  * Daniele Tricoli (eriol)
  * Molly de Blanc (mollydb)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två månaderna:

  * Nicolas Mora
  * Wolfgang Silbermayr
  * Marcos Fouces
  * kpcyrd
  * Scott Martin Leggett

Grattis!

