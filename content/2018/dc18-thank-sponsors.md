Title: DebConf18 thanks its sponsors!
Slug: dc18-thanks-its-sponsors
Date: 2018-08-02 12:00
Author: Laura Arjona Reina
Tags: debconf18, debconf, sponsors
Status: published

![DebConf18 logo](|filename|/images/DebConf18_Horizontal_Logo_600_240.png)

DebConf18 is taking place in Hsinchu, Taiwan, from July 29th to August 5th, 2018.
It is the first Debian Annual Conference in Asia, with
over 300 attendees and major advances for Debian and for Free Software in general.

Thirty-two companies have committed to sponsor DebConf18! With a warm
"thank you", we'd like to introduce them to you.

Our Platinum sponsor is  [**Hewlett Packard Enterprise (HPE)**](http://www.hpe.com/engage/opensource).
HPE is an industry-leading technology company
providing a comprehensive portfolio of products such as
integrated systems, servers, storage, networking and software.
The company offers consulting, operational support, financial services,
and complete solutions for many different industries: mobile and IoT,
data & analytics and the manufacturing or public sectors among others.

HPE is also a development partner of Debian,
and providing hardware for port development, Debian mirrors, and other Debian services
(hardware donations are listed in the [Debian machines](https://db.debian.org/machines.cgi) page).

We have four Gold sponsors:

* [**Google**](http://google.com), a technology company
specialized in Internet-related services as online advertising and search engine,
* [**Infomaniak**](https://www.infomaniak.com/), Switzerland's largest web-hosting company,
also offering live-streaming and video on demand services,
* [**Collabora**](https://www.collabora.com/),
which offers a comprehensive range of services to help its clients to
navigate the ever-evolving world of Open Source, and
* [**Microsoft**](https://www.microsoft.com/), the American multinational technology company
developing, licensing and selling computer software, consumer electronics,
personal computers and related services.

As Silver sponsors we have
[**credativ**](http://www.credativ.de/)
(a service-oriented company focusing on open-source software and also a
[Debian development partner](https://www.debian.org/partners/)),
[**Gandi**](https://www.gandi.net/)
(a French company providing domain name registration, web hosting, and related services),
[**Skymizer**](https://skymizer.com/)
(a Taiwanese company focused on compiler and virtual machine technology),
[**Civil Infrastructure Platform**](https://www.cip-project.org/),
(a collaborative project hosted by the Linux Foundation,
establishing an open source “base layer” of industrial grade software),
[**Brandorr Group**](https://www.brandorr.com/),
(a company that develops, deploys and manages new or existing infrastructure in the cloud
for customers of all sizes),
[**3CX**](https://www.3cx.com/),
(a software-based, open standards IP PBX that offers complete unified communications),
[**Free Software Initiative Japan**](https://www.fsij.org/),
(a non-profit organization dedicated to supporting Free Software growth and development),
[**Texas Instruments**](http://www.ti.com/) (the global semiconductor company),
the [**Bern University of Applied Sciences**](https://www.bfh.ch/)
(with over [6,800](https://www.bfh.ch/en/bfh/facts_figures.html) students enrolled,
located in the Swiss capital),
[**ARM**](https://www.arm.com/),
(a multinational semiconductor and software design company, designers of the ARM processors),
[**Ubuntu**](https://www.canonical.com/),
(the Operating System delivered by Canonical),
[**Cumulus Networks**](https://cumulusnetworks.com/),
(a company building web-scale networks using innovative, open networking technology),
[**Roche**](https://code4life.roche.com/),
(a major international pharmaceutical provider and research company
dedicated to personalized healthcare)
and [**Hudson-Trading**](http://www.hudson-trading.com/),
(a company researching and developing automated trading algorithms
using advanced mathematical techniques).


[**ISG.EE**](http://isg.ee.ethz.ch/),
[**Univention**](https://www.univention.com/)
[**Private Internet Access**](https://privateinternetaccess.com/),
[**Logilab**](https://www.logilab.fr/),
[**Dropbox**](https://www.dropbox.com/) and
[**IBM**](https://www.ibm.com/)
are our Bronze sponsors.

And finally, [**SLAT (Software Liberty Association of Taiwan)**](https://slat.org/),
[**The Linux foundation**](https://www.linuxfoundation.org/),
[**deepin**](https://www.deepin.com/),
[**Altus Metrum**](https://altusmetrum.org/),
[**Evolix**](https://evolix.com/),
[**BerryNet**](https://github.com/DT42/BerryNet) and
[**Purism**](https://puri.sm/)
are our supporter sponsors.

Thanks to all our sponsors for their support!
Their contributions made possible that a large number
of Debian contributors from all over the globe work together,
help and learn from each other in DebConf18.
