Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng bảy và tám 2018)
Slug: new-developers-2018-08
Date: 2018-09-09 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published


Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

  * William Blough (bblough)
  * Shengjing Zhu (zhsj)
  * Boyuan Yang (byang)
  * Thomas Koch (thk)
  * Xavier Guimard (yadd)
  * Valentin Vidic (vvidic)
  * Mo Zhou (lumin)
  * Ruben Undheim (rubund)
  * Damiel Baumann (daniel)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

  * Phil Morrell
  * Raúl Benencia
  * Brian T. Smith
  * Iñaki Martin Malerba
  * Hayashi Kentaro
  * Arnaud Rebillout

Xin chúc mừng!
